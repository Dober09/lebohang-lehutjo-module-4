import 'package:appoftheyear/pages/activities.dart';
import 'package:appoftheyear/pages/editprofile.dart';
import 'package:flutter/material.dart';
import 'package:appoftheyear/pages/dashboard.dart';
import 'package:appoftheyear/pages/home.dart';
import 'package:appoftheyear/pages/login.dart';
import 'package:appoftheyear/pages/profile.dart';
import 'package:appoftheyear/pages/register.dart';


import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DutyBuddies',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(
        seconds: 7,
        navigateAfterSeconds: const SampleAppPage(),
        backgroundColor: Colors.deepOrangeAccent,
        title: const Text(
          'AppoftheYear',
          style: TextStyle(
              fontFamily: "Ubuntu",
              letterSpacing: 2,
              fontSize: 30,
              color: Colors.white),
        ),
      ),
      routes: {
        '/home': (context) => const SampleAppPage(),
        '/login': (context) => const LoginPage(),
        '/register': (context) => const RegisterPage(),
        '/dashboard': (context) => const DashBoardPage(),
        '/profile': (context) => const ProfilePage(),
        '/feature': (context) => const Activities(),
        '/updateprofile': (context) => const EditProfile()
      },
    );
  }
}
