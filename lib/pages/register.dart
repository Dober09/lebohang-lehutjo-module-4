import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text(
          'SignUp',
          style: TextStyle(fontFamily: 'Ubuntu'),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            width: 300,
            child: TextField(
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.deepOrangeAccent),
                hintText: 'Enter Username',
                border: OutlineInputBorder(),
                icon: Icon(
                  Icons.person,
                  color: Colors.deepOrangeAccent,
                  size: 40,
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 300,
            child: TextField(
              // cursorHeight: 25,
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.deepOrangeAccent),
                hintText: 'Enter Email',
                border: OutlineInputBorder(),
                icon: Icon(
                  Icons.mail,
                  color: Colors.deepOrangeAccent,
                  size: 40,
                ),
              ),
            ),
          ),
          // const SizedBox(
          //   height: 30,
          // ),

          const SizedBox(
            width: 300,
            child: TextField(
              // cursorHeight: 30,
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.deepOrangeAccent),
                hintText: 'Enter Password',
                border: OutlineInputBorder(),
                icon: Icon(
                  Icons.lock,
                  color: Colors.deepOrangeAccent,
                  size: 40,
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Signed up Alreardy? click'),
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/login');
                  },
                  child: const Text('here'),
                ),
              ],
            ),
          ),

          SizedBox(
            width: 290,
            height: 40,
            child: OutlinedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/feature');
              },
              style: OutlinedButton.styleFrom(
                  backgroundColor: Colors.deepOrangeAccent,
                  elevation: 20,
                  side: const BorderSide(
                    color: Colors.deepOrangeAccent,
                  )),
              child: const Text(
                'SignUp',
                style: TextStyle(
                    fontFamily: 'Ubuntu',
                    color: Color.fromARGB(215, 255, 255, 255)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
