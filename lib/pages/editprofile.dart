import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Edit Profile',
          style: TextStyle(fontFamily: 'Ubuntu'),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          const SizedBox(
            width: 200,
            child: TextField(
              // cursorHeight: 30,

              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.deepOrangeAccent),
                hintText: 'Enter Username',

                // border: Outline,
              ),
            ),
          ),
          const Divider(
            height: 20,
            color: Colors.white,
          ),
          const SizedBox(
            width: 200,
            child: TextField(
              // cursorHeight: 30,
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.deepOrangeAccent),
                hintText: 'Enter Email',
                // border: Outline,
              ),
            ),
          ),
          const Divider(
            height: 20,
            color: Colors.white,
          ),
          FloatingActionButton(
            onPressed: () {
              Navigator.pushNamed(context, '/profile');
            },
            child: const Icon(Icons.upload_outlined),
          )
        ],
      ),
    );
  }
}
