import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SignIn'),
        elevation: 0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const SizedBox(
            width: 300,
            child: TextField(
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.deepOrangeAccent),
                border: OutlineInputBorder(),
                hintText: 'Enter Email',
                icon: Icon(
                  Icons.mail,
                  color: Colors.deepOrangeAccent,
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 300,
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter Password',
                  hintStyle: TextStyle(color: Colors.deepOrangeAccent),
                  icon: Icon(
                    Icons.lock,
                    color: Colors.deepOrangeAccent,
                  )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Not signed up yet? click'),
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/register');
                  },
                  child: const Text('here'),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 290,
            height: 40,
            child: OutlinedButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/dashboard');
              },
              style: OutlinedButton.styleFrom(
                  backgroundColor: Colors.deepOrangeAccent,
                  elevation: 20,
                  side: const BorderSide(
                    color: Colors.deepOrangeAccent,
                  )),
              child: const Text(
                'SignIn',
                style: TextStyle(
                    color: Color.fromARGB(215, 255, 255, 255),
                    fontFamily: 'Ubuntu'),
              ),
            ),
          )
        ],
      ),
    );
  }
}
