import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'My Profile',
          style:
              TextStyle(fontSize: 34, letterSpacing: 1, fontFamily: 'Ubuntu'),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            const Divider(
              height: 20,
              color: Colors.white,
            ),
            const Icon(
              Icons.account_circle,
              color: Colors.deepOrangeAccent,
              size: 70,
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: const [
                  Text(
                    'Username',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  Text('john'),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: const [
                  Text(
                    'Email',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  Text('john@mail'),
                ],
              ),
            ),
            Container(
              width: 200,
              margin: const EdgeInsets.symmetric(vertical: 30),
              child: OutlinedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/updateprofile');
                },
                child: const Text('Edit'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
